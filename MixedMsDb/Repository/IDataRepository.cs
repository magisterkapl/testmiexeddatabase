﻿using System.Collections.Generic;

namespace MixedMsDb.Repository
{
    public interface IDataRepository<T, U> where T : class
    {
        IEnumerable<T> All { get; }
        T Get(U id);
        int Add(T b);
        int Update(U id, T b);
        int Delete(U id);
    }
}
