﻿using MixedMsDb.Models;
using System;
using System.Collections.Generic;

namespace MixedMsDb.Repository
{
    public class TestRepository : IDataRepository<Test, int>
    {
        MixedMsDbContext _ctx;

        public TestRepository(MixedMsDbContext ctx) => _ctx = ctx;

        public IEnumerable<Test> All => _ctx.Tests;

        public int Add(Test b)
        {
            throw new NotImplementedException();
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Test Get(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(int id, Test b)
        {
            throw new NotImplementedException();
        }
    }
}
