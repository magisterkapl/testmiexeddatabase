﻿using MixedMsDb.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace MixedMsDb
{
    public class MixedMsDbContext : IdentityDbContext<IdentityUser>
    {
        public MixedMsDbContext(DbContextOptions<MixedMsDbContext> options) : base(options) { }

        public DbSet<Test> Tests { get; set; }
    }
}
