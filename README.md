# README #

docker load --input .\mssqlimage.tar
Loaded image: mixed/mssql:version2

docker images
REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
mixed/mssql                version2            262dfde28225        12 minutes ago      1.58GB
docker4w/nsenter-dockerd   latest              cae870735e91        6 months ago        187kB

docker run -p 1401:1433 -d --name=mssql mixed/mssql:version2
docker ps -a
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS              PORTS                    NAMES
11fc2ad46951        mixed/mssql:version2   "/opt/mssql/bin/sqls�"   16 seconds ago      Up 15 seconds       0.0.0.0:1401->1433/tcp   mssql

docker inspect --format '{{.NetworkSettings.Networks.nat.IPAddress}}' mssql
<no value>

docker stop mssql
mssql

docker start mssql
mssql

SERVER NAME localhost,1401
LOGIN sa
LOGIN docker
PASSWORD d0cker123@

database -> mixedmsdbt

# DYSK #

https://drive.google.com/drive/folders/1PeK38tIcPcbUPDxETks6vihXka16e2Cd?usp=sharing