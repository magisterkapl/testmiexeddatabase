import '@cgross/angular-notify/dist/angular-notify.css';

import angular from 'angular';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';



import 'angular-ui-bootstrap';
import 'normalize.css';


import uirouter from 'angular-ui-router';
import { routing, routingEventsLogger } from './app.config';
import './app.component.scss';
import requestFactory from './app.request.factory.js';
import globalFactory from './app.global.factory.js';
import aUiBootstrap from 'angular-ui-bootstrap';
import AppCtrl from './app.controller.js';
import aNotify from '@cgross/angular-notify';
import aCache from 'angular-cache';

import support from './components/support/support.module';
import login from './components/login/login.module';

// export for others scripts to use


const DEBUG = true;

angular
    .module('app', [uirouter, support,login,aUiBootstrap,aNotify,aCache])
    .service('RequestFactory', requestFactory)
    .service('GlobalFactory', globalFactory)
    .controller('AppCtrl', AppCtrl)
    .config(routing)
    .run(function ($state, $q, CacheFactory,$rootScope) {
        
        var _cacheUser = CacheFactory.get('userCache');

        var _initCacheUser = function(){

            return $q(function(resolve, reject) {
                if(!_cacheUser){
                    console.log('robie');
                    _cacheUser = CacheFactory('userCache', {
                        maxAge: 15 * 60 * 1000,
                        cacheFlushInterval: 60 * 60 * 1000,
                        deleteOnExpire: 'aggressive',
                        storageMode: 'localStorage'
                    });
                    resolve(true);

                } else {
                    resolve(false);
                }
            })
        };

        _initCacheUser().then(function (response) {
            if(response && _cacheUser.get('isLogged')){
                $rootScope.r_isLogged = true;
                console.log($rootScope.r_isLogged);
                $state.go('testy.lista');
            } else {
                $rootScope.r_isLogged = false;

            }
        });
    });
//
// if (DEBUG) {
//     app
//         .run(routingEventsLogger)
//     ;
// }