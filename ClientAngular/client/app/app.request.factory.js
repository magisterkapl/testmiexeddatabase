function RequestFactory($http,$q)
{
  const requestService = this;

  requestService.getActiveBooks = function(){
    return $http.get('http://localhost:5000/api/values').then(result => result.data );
  };

  requestService.loginUser = function(pass){
      console.log(pass);
      //przykład promissa zwracany w przypadku odpowiedzi przez backend
      return $q(function(resolve, reject) {
          if(pass.login == 'aa' && pass.password == 'aa'){
              resolve(true);
          } else {
              resolve(false);
          }
      });

      // return $http.get('http://localhost:5000/api/values')
      //     .then(result => result.data );

  };

}

export default RequestFactory;