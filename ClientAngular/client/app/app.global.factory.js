function GlobalFactory($http,$q,notify)
{
    const globalService = this;

    globalService.notify = function(message,classes,duration){

        if(angular.isUndefined(classes))
            classes = "alert-info";
        if(angular.isUndefined(duration))
            duration = 5000;

        var notifySettings = {message:message, classes:classes, duration:duration};
        notify(notifySettings);
    };

}

export default GlobalFactory;