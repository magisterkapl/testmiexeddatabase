import template from './support.tpl.html'

export function routing($stateProvider) {

    $stateProvider
        .state('support', {
            abstract : true,
            url: '/support',
            template: '<div ui-view></div>',
            parent: 'app'
        })
        .state('support.zgloszenia', {
            url: '/zgloszenia',
            controller: 'SupportCtrl',
            controllerAs: 'ctrl',
            template: template
        });
}