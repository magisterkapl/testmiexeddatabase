import angular from 'angular';

import { routing } from './support.config.js';
import SupportCtrl from './support.controller.js';

export default angular.module('wojtek', [])
    .config(routing)
    .controller('SupportCtrl', SupportCtrl)
    .name;