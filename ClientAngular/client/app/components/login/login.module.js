import angular from 'angular';
import aCache from 'angular-cache';
import aChart from 'angular-chart.js';


import { routing } from './login.config.js';
import LoginCtrl from './login.controller.js';
import RejeCtrl from './rejestracja.controller.js';

import aNotify from '@cgross/angular-notify';

export default angular.module('login',[aNotify,aCache,aChart])
    .config(routing)
    .config(['ChartJsProvider', function (ChartJsProvider) {
        // Configure all charts
        ChartJsProvider.setOptions({
            colours: ['#FF5252', '#FF8A80'],
            responsive: false
        });
        // Configure all line charts
        ChartJsProvider.setOptions('Line', {
            datasetFill: false
        });
    }])
    .controller('LoginCtrl', LoginCtrl)
    .controller('RejeCtrl', RejeCtrl)
    .name;