import tplLogin from './login.tpl.html';
import tplRejestracja from './rejestracja.tpl.html';

export function routing($stateProvider) {

    $stateProvider
        .state('login', {
            abstract : true,
            url: '/',
            template: '<div ui-view></div>',
            parent: 'app'
        })
        .state('login.user', {
            url: 'enter',
            controller: 'LoginCtrl',
            template: tplLogin
        })
        .state('login.rejestracja', {
            url: 'rejestracja',
            controller: 'RejeCtrl',
            template: tplRejestracja
        });
}