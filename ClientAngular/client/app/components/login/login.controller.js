export default function LoginCtrl($http,RequestFactory,GlobalFactory,$rootScope,$scope,CacheFactory, $timeout) {

    $scope.loginData = {
        login: null,
        password : null
    };
    var _cacheUser = CacheFactory.get('userCache');

    $scope.testLogin = function(){
        $scope.value= $scope.value + 1;

        RequestFactory.loginUser($scope.loginData)
            .then(function(response){
                if(response){
                    GlobalFactory.notify('Witaj ' + $scope.loginData.login + ' !','alert-succes',3000);
                    $rootScope.r_isLogged = true;
                    _cacheUser.put('isLogged',true);
            } else {
                    GlobalFactory.notify('Błędne dane.','alert-danger',3000);
                    _cacheUser.put('isLogged',false);
                }

        });
    }

    $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
    $scope.series = ['Series A', 'Series B'];
    $scope.data = [
        [65, 59, 80, 81, 56, 55, 40],
        [28, 48, 40, 19, 86, 27, 90]
    ];
    $scope.onClick = function (points, evt) {
        console.log(points, evt);
    };

    // Simulate async data update
    $timeout(function () {
        $scope.data = [
            [28, 48, 40, 19, 86, 27, 90],
            [65, 59, 80, 81, 56, 55, 40]
        ];
    }, 3000);

    $scope.labels1 = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
    $scope.data1 = [300, 500, 100];

    $scope.colors2 = ['#45b7cd', '#ff6384', '#ff8e72'];

    $scope.labels2 = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    $scope.data2 = [
        [65, -59, 80, 81, -56, 55, -40],
        [28, 48, -40, 19, 86, 27, 90]
    ];
    $scope.datasetOverride2 = [
        {
            label: "Bar chart",
            borderWidth: 1,
            type: 'bar'
        },
        {
            label: "Line chart",
            borderWidth: 3,
            hoverBackgroundColor: "rgba(255,99,132,0.4)",
            hoverBorderColor: "rgba(255,99,132,1)",
            type: 'line'
        }
    ];

}