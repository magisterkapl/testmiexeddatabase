﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MixedMsDb.Models;
using MixedMsDb.Repository;
using System.Collections.Generic;

namespace TestMiexedDatabase.Api.Query
{
    [Route("api/Query/[controller]")]
    public class ValuesController
    {
        private readonly IDataRepository<Test, int> _iRepo;
        public ValuesController(IDataRepository<Test, int> iRepo) => _iRepo = iRepo;

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            //var a = _iRepo.All;
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        [Authorize]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
    }
}
